#!/bin/bash

#Eliminamos los contenedores en caso de que existan
sudo ip link set bridge0 down
sudo ip link del bridge0
sudo ip link del veth11 && sudo ip link del veth12
sudo ip link del veth21 && sudo ip link del veth22

docker stop coneusta && docker rm coneusta
docker stop coneusta2 && docker rm coneusta2

docker build -t "eusta:Dockerfile" .

docker run -itd --net=none --name="coneusta" eusta:Dockerfile
docker run -itd --net=none --name="coneusta2" eusta:Dockerfile

sudo mkdir -p /var/run/netns
sudo ln -sf /proc/$(docker inspect coneusta -f '{{.State.Pid}}')/ns/net /var/run/netns/coneusta
sudo ln -sf /proc/$(docker inspect coneusta2 -f '{{.State.Pid}}')/ns/net /var/run/netns/coneusta2

sudo ip link add bridge0 type bridge

sudo ip link add veth11 type veth peer name veth12
sudo ip link add veth21 type veth peer name veth22

sudo ip link set netns coneusta dev veth11
sudo ip link set netns coneusta2 dev veth21

sudo ip link set veth12 master bridge0
sudo ip link set veth22 master bridge0

sudo ip addr add 172.16.1.10/26 dev bridge0

sudo ip netns exec coneusta ip addr add 172.16.1.1/26 dev veth11
sudo ip netns exec coneusta2 ip addr add 172.16.1.2/26 dev veth21

sudo ip netns exec coneusta ip link set veth11 up
sudo ip netns exec coneusta2 ip link set veth21 up

sudo ip link set veth12 up
sudo ip link set bridge0 up
sudo ip link set veth22 up

sudo ip netns exec coneusta ip route add default via 172.16.1.10
sudo ip netns exec coneusta2 ip route add default via 172.16.1.10


sudo iptables -F FORWARD

sudo iptables -P FORWARD ACCEPT
sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -s 172.16.1.0/26 ! -o bridge0 -j MASQUERADE


sudo ip route add 172.16.1.0/26 via 192.168.0.176
