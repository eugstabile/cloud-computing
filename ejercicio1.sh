#!/bin/bash

#Iniciar contenedor
docker run --net=none --name con_eusta -itd ubuntu:latest /bin/bash

#Hacer los namespaces de los contenedores visibles
	sudo ln -sf /proc/"$(docker inspect con_eusta -f '{{.State.Pid}}')"/ns/net /var/run/netns/con_eusta

#Creación Veth
	sudo ip link add veth00 type veth peer name veth01

#Añadir veth a los contenedores
	sudo ip link set netns "$(docker inspect con_eusta -f '{{.State.Pid}}')" dev veth00

#Inicializar el link creado desde el contenedor
	 sudo nsenter -t "$(docker inspect con_eusta -f '{{.State.Pid}}')" -n ip link set veth00 up

#Asignar ip a veth 
	 sudo nsenter -t "$(docker inspect con_eusta -f '{{.State.Pid}}')" -n ip address add 10.10.10.1/24 dev veth00
