# Cloud Computing - Construcción manual de una red entre contenedores

## DockerFile

- Este trabajo se realizó con un Dockerfile para el despliegue de los contenedores con unas determinadas caracteristicas adicionales.

```Dockerfile
FROM ubuntu:latest
RUN apt-get -y update
RUN apt-get install -y iputils-ping
RUN apt-get install -y iproute2
```                           
- Partiendo de una imagen de ubuntu, se actualizó el gestor de paquetes para posteriormente añadir las utilidades de iproute2 (utilización del comando ip) e iputils-ping (comando ping).

## Ejercicio 1

### La estructura del siguiente ejercicio será la de unir dos contenedores ubicados en *namespaces* diferentes mediante un par VETH . Para ello ejecutaremos los siguientes comandos por la terminal de linux. 


- Crear dos contenedores sin *network*.

```bash
docker run --net=none --name con_eusta -itd ubuntu:latest /bin/bash
docker run --net=none --name con_eusta2 -itd ubuntu:latest /bin/bash
```

- Hacemos los *namespaces* de los contenedores visibles en el *host*  utilizando $(docker inspect nombre_contenedor -f '{{.State.Pid}}') para identificar el contenedor que queremos utilizar.

```bash
sudo ln -sf /proc/"$(docker inspect con_eusta -f '{{.State.Pid}}')"/ns/net /var/run/netns/con_eusta
sudo ln -sf /proc/"$(docker inspect con_eusta2 -f '{{.State.Pid}}')"/ns/net /var/run/netns/con_eusta2
```

- Creación de un par VETH.
 
```bash
sudo ip link add veth00 type veth peer name veth01
```

- Añadir cada conector VETH a los contenedores correspondientes.

```bash
sudo ip link set netns "$(docker inspect con_eusta -f '{{.State.Pid}}')" dev veth00
sudo ip link set netns "$(docker inspect con_eusta2 -f '{{.State.Pid}}')" dev veth01
```

- Inicializar el link creado desde el contenedor.

```bash
sudo nsenter -t "$(docker inspect con_eusta -f '{{.State.Pid}}')" -n ip link set veth00 up
sudo nsenter -t "$(docker inspect con_eusta2 -f '{{.State.Pid}}')" -n ip link set veth01 up
```

- Asignar una dirección IP a VETH de forma que los dos contenedores obtengan una dirección IP que se encuentre dentro de la subred.

```bash
sudo nsenter -t "$(docker inspect con_eusta -f '{{.State.Pid}}')" -n ip address add 172.16.10.1/24 dev veth00
sudo nsenter -t "$(docker inspect con_eusta2 -f '{{.State.Pid}}')" -n ip address add 172.16.10.2/24 dev veth01
```
- Por último, podremos realizar desde uno de los contenedores un ping a la dirección que le hemos asignado al otro contenedor (ej. coneusta)

```bash
sudo ip netns exec coneusta ping 172.16.10.2
```

## Ejercicio 2

### La estructura del siguiente ejercicio será la de unir dos contenedores ubicados en *namespaces* diferentes a un *bridge* situado en el *namespace* del *host*  y que se puedan comunicar estos nodos entre sí y con el host.

- Creamos un contenedor que contenga las aplicaciones necesarias para poder utilizar los comandos ping e ip. 

```bash
docker build -t "eusta:Dockerfile" .
```
- Creamos los contenedores sin *network* y con un determinado nombre.

```bash
docker run -itd --net=none --name="coneusta" eusta:Dockerfile
docker run -itd --net=none --name="coneusta2" eusta:Dockerfile
```
-  Crear *namespaces* visible para los contenedores.

```bash
sudo mkdir -p /var/run/netns
sudo ln -sf /proc/$(docker inspect coneusta -f '{{.State.Pid}}')/ns/net /var/run/netns/coneusta
sudo ln -sf /proc/$(docker inspect coneusta2 -f '{{.State.Pid}}')/ns/net /var/run/netns/coneusta2
```

- Creamos tanto un *bridge* como 2 pares de conectores VETH.

```bash
sudo ip link add bridge0 type bridge
sudo ip link add veth11 type veth peer name veth12
sudo ip link add veth21 type veth peer name veth22
```
- Añadimos a los extremos de los *namespaces* su VETH correspondiente.

```bash
sudo ip link set netns coneusta dev veth11
sudo ip link set netns coneusta2 dev veth21
```
- Añadimos al *brigde*  los otros extremos de los VETH correspondientes a los *namespaces*.

```bash
sudo ip link set veth12 master bridge0
sudo ip link set veth22 master bridge0
```

- Asignamos direcciones IP a las conexiones VETH en ambos contenedores y al *brigde* de forma que coincidan en la misma subred.

```bash
sudo ip addr add 172.16.1.10/26 dev bridge0
sudo ip netns exec coneusta ip addr add 172.16.1.1/26 dev veth11
sudo ip netns exec coneusta2 ip addr add 172.16.1.2/26 dev veth21
```
- Iniciamos las conexiones VETH en ambos contenedores. 

```bash
sudo ip netns exec coneusta ip link set veth11 up
sudo ip netns exec coneusta2 ip link set veth21 up
```
- Inicializamos las VETH ligadas al *bridge* y el *bridge*.

```bash
sudo ip link set veth12 up
sudo ip link set bridge0 up
sudo ip link set veth22 up
```
- Asignamos a las interfaces de los contenedores la puerta de enlace por defecto. En este caso le asignaremos que los paquetes se envíen al *bridge0*.

```bash
sudo ip netns exec coneusta ip route add default via 172.16.1.10
sudo ip netns exec coneusta2 ip route add default via 172.16.1.10
```

## Ejercicio 3

### La estructura del siguiente ejercicio será la de unir dos contenedores ubicados en *namespaces* diferentes a un *bridge* situado en el *namespace* del *host* y que se puedan comunicar estos nodos a los mismos nodos alcanzados por el *host*. Para ello reautilizamos el código correspondiente al ejercicio anterior y añadiremos lo siguiente.

- Nos aseguramos de que se puedan enviar los paquetes necesarios para establecer la conexión deseada. Para ello realizaremos en el *host* lo siguiente:

```bash
sudo iptables -F FORWARD
sudo iptables -P FORWARD ACCEPT
sudo sysctl -w net.ipv4.ip_forward=1
```

- Indicamos en la tabla nat que se envíen y obtengan paquetes correspondientes de internet en la subred elegida.

```bash
sudo iptables -t nat -A POSTROUTING -s 172.16.1.0/26 ! -o bridge0 -j MASQUERADE
```

## Ejercicio 4

### La estructura del siguiente ejercicio será la de comunicar dos contenedores ubicados en nodos diferentes, para ello utilizaremos el esquema de los anteriores ejercicios 2 y 3 añadiendo además la siguiente linea de código:

- Creamos un enrutamiento estático para la subred creada.

```bash
sudo ip route add 172.16.1.0/26 via 192.168.0.176
```

Siendo la dirección 192.168.0.176 la ip del *host* donde se encuentra los contenedores. Para que la conexión sea bidireccional, en el *host* con la dirección anterior habrá que ejecutar el mismo comando pero poniendo la ip del otro *host*. Cabe mencionar que se deberá de crear en los nodos siguientes a desplegar el mismo esquema para crear los contenedores con sus correspondientes *namespace* y sus conexiones VETH,  teniendo siempre en cuenta que no hayan duplicidades en la asignación de las direcciones IP para evitar coliciones.


